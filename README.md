# Todo Application using Node.js Express

This repository contains a simple Todo application built using Node.js and Express. It includes basic CRUD operations for managing tasks.

## Installation

1. Clone the repository to your local machine:

git clone https://gitlab.com/GitChetan/worldline_first_assessmentt.git


2. Navigate to the project directory:

cd todo-app


3. Install dependencies:

npm install
npm i Express


4. Start the server:

node app.js


The server will start running on `http://localhost:3000`.

## Usage

- Navigate to `http://localhost:3000` in your web browser.
- You can add, view tasks in the Todo list.

## Continuous Integration/Continuous Deployment (CI/CD) Pipeline

This project includes a CI/CD pipeline implemented using GitLab CI/CD.

### Setup GitLab Repository

1. Create a new GitLab repository (public) for this assignment.
2. Clone the repository to your local machine.

### Sample Web Application

- Created a simple web application using Node.js and Express.
- Included basic CRUD functionality for managing tasks.
- Committed and pushed the code to the GitLab repository.

### Configure GitLab CI/CD Pipeline

1. Created a `.gitlab-ci.yml` file in the root of the repository.
2. Defined stages such as build and deploy.
3. Implemented jobs for each stage:
   - **Build:** Set up the environment and build the application.
   - **Deploy:** Deploy the application to a staging environment.
4. Utilized GitLab CI/CD features like caching dependencies, environment variables, and artifacts.

### Deployment

- Configured the deployment job to automatically deploy the application to the staging environment upon successful testing.
- Ensured that the deployed application is accessible and functional.

### Documentation

- Documented the steps to set up the CI/CD pipeline in this README.md file.
- Included prerequisites, setup instructions, and usage details.

## Contributors

- Chetan Patil
